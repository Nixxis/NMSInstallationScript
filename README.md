# **Nixxis MediaServer Script**

> **Nixxis MediaServer Script** is a shell script that allows you to install all the needed component for Nixxis one a Centos 6.x environment. 

## Download Script

As root, download the script and patches files on the server to install

```bash
sudo -s
wget https://gitlab.com/Nixxis/NMSInstallationScript/raw/master/nms.sh
wget https://gitlab.com/Nixxis/NMSInstallationScript/raw/master/AgiExitOnHangup.patch
```

Grant execution permissions on the script

```bash
chmod +x nms.sh
```

Execute the script

```bash
./nms.sh
```

## Install Nixxis Coponement

> The first option for this script is for performing the installation of Media Server

### Execute

To perform the installation, run the script with argument **install**

```bash
./nms.sh -install
```

The script will execute various operation that can be séparate in 5 steps

#### Get configuration

#### Check prerequisites and initial setup

#### Installing needed tools

#### Installing Astreisk

#### Deploying Nixxis configuration and Dialplans

## Configure Nixxis MediaServer

> The second option for this script is for performing the configuration of Media Server in order to work with the Application Server.
> This include mount and sip configuration.

### Execute

To perform the configuration, run the script with argument **config**

```bash
./nms.sh -config
```

The script will execute various operation that can be séparate in 3 steps

#### Get configuration

#### Create SIP configuration file 

#### Create mount folder

## Check Nixxis MediaServer

> The third option for this script is for performing the checkup of Media Server.

### Execute

To perform the checkup, run the script with argument **check**

```bash
./nms.sh -check
```

The script will check if every thing is well installed and running and will print some realtime information on the status of the media server.